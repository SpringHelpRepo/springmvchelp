import configFiles.messageProviderConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import service.MessageProvider;


public class hibernate {

    @Autowired()
    static MessageProvider provider;

    public static void main(String[] args ) {
        provider.printMessage();
    }
}
