package service;

public class MessageProviderImpl implements MessageProvider {

    private String message;

    public String getMessage() {
        return this.message;
    }

    public void printMessage() {
        System.out.println(this.message);
    }

    public MessageProviderImpl(String message) {
        this.message = message + " ,Howdy there!";
    }

}
