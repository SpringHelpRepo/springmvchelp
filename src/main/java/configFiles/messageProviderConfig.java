package configFiles;


import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import service.MessageProvider;
import service.MessageProviderImpl;

@Configuration
public class messageProviderConfig {

    private static final Logger logger = LoggerFactory
            .getLogger(messageProviderConfig.class);

    @Bean
    @Qualifier("MessageProvider")
    public MessageProvider getMessageProvider() {
        logger.debug("Message Provider bean instantiated");
        return new MessageProviderImpl("Welcome!");
    }

    //@Bean
    public MessageProvider getMessageProviderAngry() {
        logger.debug("Message Provider Angry bean instantiated");
        return new MessageProviderImpl("Welcome!");
    }

}
