package servlets;

import configFiles.messageProviderConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;


class DispatcherServlet implements WebApplicationInitializer {


    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext rootContext  = new AnnotationConfigWebApplicationContext();
        rootContext.register(messageProviderConfig.class);
        servletContext.addListener(new ContextLoaderListener((WebApplicationContext) rootContext));
    }


}
